"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
import app_friend.urls as app_friend
import app_profile.urls as app_profile
import app_dashboard.urls as app_dashboard
import app_status.urls as app_status
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^friend/', include(app_friend, namespace='add-friend')),
    url(r'^dashboard/', include(app_dashboard, namespace='dashboard')),
    url(r'^app-profile/', include(app_profile,namespace='app-profile')),
    url(r'^app_status/', include(app_status, namespace='app_status')),
    url(r'^$', RedirectView.as_view(url="/app_status/", permanent="true"), name='app_status'),

]
