from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from app_friend.models import Friend
from app_status.models import Status

# Create your tests here.
class DashboardUnitTest(TestCase):
	def test_dashboard_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)

	def test_number_of_friend_on_dashboard_is_right(self):
		new_friend = Friend.objects.create(name='test', url='http://test.com')
		number_of_friend = str(Friend.objects.all().count())
		response= Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(number_of_friend, html_response)

	def test_number_of_post_on_dashboard_is_right(self):
		new_status = Status.objects.create(message='coba ya')
		number_of_status = str(Status.objects.all().count())
		response= Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(number_of_status, html_response)

	def test_latest_post(self):
		first = 'pertama'
		last = 'terakhir'
		Status.objects.create(message=first)
		Status.objects.create(message=last)
		latest_post = Status.objects.last().message
		self.assertEqual(last, latest_post)
