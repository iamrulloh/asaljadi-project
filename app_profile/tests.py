from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Profile

# Create your tests here.

class appProfileUnitTest(TestCase):

    def test_app_profile_url_is_exist(self):
        response = Client().get('/app-profile/')
        self.assertEqual(response.status_code, 200)

    def test_appProfile_using_index_func(self):
        found = resolve('/app-profile/')
        self.assertEqual(found.func, index)

    def test_model_can_create_profile(self):
        profile = Profile.objects.create(name='Mr. Test',gender='Male', description='Mr. Test description ', email='test@test.com')

        counting_all_available_profile = Profile.objects.all().count()
        self.assertEqual(counting_all_available_profile,1)
