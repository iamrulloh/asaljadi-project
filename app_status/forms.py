from django import forms
# Create your views here.
class Message_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, max_length=100)
